# Labour code widgets

## Description

Allow to interface with labour_code widget from French gouvernment.

## Installation

* Install and enable this module like any other Drupal 8, 9 or 10 module.

## Configuration

In the settings page (/admin/structure/labour-code-widgets/status),
you can configure which widgets are enabled / disabled.
All the widgets are enabled by default.

### How it works

- Simply add the new field type 'Labour code widgets field' to your entity.
- Configure cardinality
- Contribute wanted widgets.

The contributed widget will now be in your page.

## Maintainers

* Joris Simonet (jsimonet) - https://www.drupal.org/user/3731549
