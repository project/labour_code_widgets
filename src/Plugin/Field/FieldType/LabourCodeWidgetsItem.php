<?php

declare(strict_types = 1);

namespace Drupal\labour_code_widgets\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;

/**
 * Provides a field type for labour_code_widgets.
 *
 * @FieldType(
 *   id = "labour_code_widgets_field",
 *   label = @Translation("Labour code widgets"),
 *   default_formatter = "labour_code_widgets_formatter",
 *   default_widget = "options_select",
 * )
 */
class LabourCodeWidgetsItem extends ListStringItem {

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(?AccountInterface $account = NULL): array {
    return \Drupal::service('labour_code_widgets.helper')->getOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    return [];
  }

}
