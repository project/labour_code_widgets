<?php

declare(strict_types = 1);

namespace Drupal\labour_code_widgets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\labour_code_widgets\Helper\LabourCodeWidgetsHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the labour_code_widgets formatter.
 *
 * @FieldFormatter(
 *   id = "labour_code_widgets_formatter",
 *   label = "Labour code widgets",
 *   field_types = {
 *     "labour_code_widgets_field"
 *   }
 * )
 */
class LabourCodeWidgetsFieldFormatter extends FormatterBase {

  /**
   * Helper for labour code widgets.
   *
   * @var \Drupal\labour_code_widgets\Helper\LabourCodeWidgetsHelperInterface
   */
  private LabourCodeWidgetsHelperInterface $widgetHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiate formatter class.
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->widgetHelper = $container->get('labour_code_widgets.helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      (string) $this->t('Display labour code widget'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $widgets = [];
    // Prepare widgets' template.
    foreach ($items as $delta => $item) {
      $widgetDefinition = $this->widgetHelper->getWidgetDefinition($item->value);
      $widgets[$delta] = [
        '#theme' => 'labour_code_widget',
        '#url' => LabourCodeWidgetsHelperInterface::BASE_URL . $widgetDefinition['uri'],
        '#label' => $widgetDefinition['label'],
      ];
    }

    // Attach library which render widgets.
    if (!$items->isEmpty()) {
      $widgets['#attached']['library'][] = 'labour_code_widgets/widget';
    }

    return $widgets;
  }

}
