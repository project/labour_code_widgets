<?php

declare(strict_types = 1);

namespace Drupal\labour_code_widgets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\labour_code_widgets\Helper\LabourCodeWidgetsHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for labour code widgets.
 */
class LabourCodeWidgetsSettingsForm extends ConfigFormBase {

  /**
   * Settings name.
   *
   * @var string
   */
  const SETTINGS_NAME = 'labour_code_widgets.settings';

  /**
   * Helper for labour code widgets.
   *
   * @var \Drupal\labour_code_widgets\Helper\LabourCodeWidgetsHelperInterface
   */
  private LabourCodeWidgetsHelperInterface $widgetHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiate formatter class.
    $instance = parent::create($container);
    $instance->widgetHelper = $container->get('labour_code_widgets.helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'labour_code_widgets_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $disabledWidgets = $this->config(static::SETTINGS_NAME)
      ->get('disabled');

    $form['disabled'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Disabled labour code widgets'),
      '#tree' => TRUE,
    ];

    $form['disabled']['information'] = [
      '#markup' => $this->t('All widgets are enabled by default.
        You can disable specific widgets by selecting them you want to disable
        in the list above.'
      ),
    ];

    foreach ($this->widgetHelper->getWidgetsDefinition() as $widgetId => $widgetDefinition) {
      $form['disabled'][$widgetId] = [
        '#type' => 'checkbox',
        '#title' => $widgetDefinition['label'],
        '#default_value' => in_array($widgetId, $disabledWidgets),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config(static::SETTINGS_NAME)
      ->set(
        'disabled',
        Checkboxes::getCheckedCheckboxes($form_state->getValue('disabled'))
      )->save();
    parent::submitForm($form, $form_state);
  }

}
