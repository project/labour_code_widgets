<?php

declare(strict_types = 1);

namespace Drupal\labour_code_widgets\Helper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Helper class for 'code_du_travail_widget'.
 */
class LabourCodeWidgetsHelper implements LabourCodeWidgetsHelperInterface {

  use StringTranslationTrait;

  /**
   * Labour code widgets settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $widgetsSettings;

  /**
   * Labour code widgets helper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->widgetsSettings = $config_factory
      ->get('labour_code_widgets.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(): array {
    $disabledOptions = $this->widgetsSettings->get('disabled');
    $options = [];
    foreach ($this->getWidgetsDefinition() as $widgetId => $widgetDefinition) {
      if (in_array($widgetId, $disabledOptions)) {
        continue;
      }

      $options[$widgetId] = $widgetDefinition['label'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getWidgetDefinition(string $widgetId): array {
    return $this->getWidgetsDefinition()[$widgetId] ??
      ['uri' => '', 'label' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function getWidgetsDefinition(): array {
    return [
      'moteur_recherche' => [
        'uri' => '/search',
        'label' => $this->t("Numeric labour's code search engine"),
      ],
      'preavis_licenciement' => [
        'uri' => '/preavis-licenciement',
        'label' => $this->t('Notice of dismissal'),
      ],
      'preavis_retraite' => [
        'uri' => '/preavis-retraite',
        'label' => $this->t('Notice of departure or retirement'),
      ],
      'procedure_licenciement' => [
        'uri' => '/procedure-licenciement',
        'label' => $this->t('Understand your dismissal procedure'),
      ],
      'indemnite_licenciement' => [
        'uri' => '/indemnite-licenciement',
        'label' => $this->t('Severance pay'),
      ],
      'indemnite_precarite' => [
        'uri' => '/indemnite-precarite',
        'label' => $this->t('Precariousness allowance'),
      ],
      'convention_collective' => [
        'uri' => '/convention-collective',
        'label' => $this->t('Find your collective agreement'),
      ],
      'modeles_courriers_9e95f05bae' => [
        'uri' => '/modeles-de-courriers/9e95f05bae',
        'label' => $this->t('Document templates'),
      ],
    ];
  }

}
