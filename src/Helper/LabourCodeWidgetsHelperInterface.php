<?php

declare(strict_types = 1);

namespace Drupal\labour_code_widgets\Helper;

/**
 * Interface for CodeTravailWidgetHelper.
 */
interface LabourCodeWidgetsHelperInterface {

  /**
   * Define widget's base url.
   *
   * @var string
   */
  const BASE_URL = 'https://code.travail.gouv.fr/widgets';

  /**
   * Get field's options.
   *
   * @return array
   *   Options to use in widget's select field.
   */
  public function getOptions(): array;

  /**
   * Get widgets' definition (uri and label).
   *
   * @param string $widgetId
   *   Widget's id.
   *
   * @return array
   *   Get widget definition from widget id in parameter.
   */
  public function getWidgetDefinition(string $widgetId): array;

  /**
   * Get widgets definition list.
   *
   * @return array
   *   Widgets definition list.
   */
  public function getWidgetsDefinition(): array;

}
